from flask import Flask, render_template, jsonify, request
import json

app = Flask(__name__)


@app.route('/add_data', methods=['GET', 'POST'])
def add_data():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        empid = request.form['empid']

        data = {'name': name,
                'email': email,
                'empid': empid
                }

        with open('data.json', 'a') as json_file:
            json_file.write(json.dumps(data) + '\n')

        return jsonify({'message': 'Data stored Successfully'})

    else:
        return render_template('add_data.html')


@app.route('/view_data', methods=['GET', 'POST'])
def view_data():
    if request.method == 'POST':
        empid = request.form.get('empid')

        with open('data.json', 'r') as json_file:
            data = []

            for entry in json_file:
                data.append(json.loads(entry))

            matching_data = None
            for val in data:
                if val.get('empid') == empid:
                    matching_data = val
                    break

            if matching_data:
                return jsonify(matching_data)

    else:
        return render_template('view_data.html')


if __name__ == '__main__':
    app.run(debug=True)

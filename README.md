REST API in Python using Flask and JSON file.

Function:
    1. Index Page to Add, Search and Delete data.
    2. Add Page to Add info into a JSON file.
    3. View Page to Search info into JSON file.
    4. Delete Page to delete info from JSON file.